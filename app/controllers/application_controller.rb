class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  layout :layout_by_resource

  protected

  def layout_by_resource
    if get_current_controller == 'search'
      'application_search'
    else
      'application'
    end
  end

  def get_current_controller
    self.class.name.split('::').last().to_s[0..-11].underscore
  end
end
