#= require jquery
#= require handlebars
#= require ember
#= require ember-data
#= require_self
#= require ./emberjs/rails_emberjs

# for more details see: http://emberjs.com/guides/application/
window.RailsEmberjs = Ember.Application.create()

