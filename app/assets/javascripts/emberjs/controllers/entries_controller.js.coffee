RailsEmberjs.EntriesController =  Ember.Controller.extend
  entries: []

  actions:
    addEntry: ->
      @entries.pushObject name: @get('newEntryName')
      @set('newEntryName', "")